### **Description**
Concept of an app for VKH Ostrava displaying the same content as the web page.

---
### **Technology**
Android

---
### **Year**
2015

---
### **Screenshots**
![](/README/vkhApp.jpg)
![](/README/androidStudioScreen.png)

