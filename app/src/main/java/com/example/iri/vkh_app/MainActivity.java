package com.example.iri.vkh_app;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.util.LinkedList;

import data.Event;
import data.EventService;
import it.neokree.materialnavigationdrawer.MaterialNavigationDrawer;
import it.neokree.materialnavigationdrawer.elements.MaterialSection;


public class MainActivity extends MaterialNavigationDrawer {

    public static LinkedList<Event> events;

    public static Fragment_actions f_actions = new Fragment_actions();
    public static Fragment_notifications f_notifications = new Fragment_notifications();
    public static Fragment_chat f_chat = new Fragment_chat();

    /**
     * Zde se nastavuje levé vysouvací menu
     */
    @Override
    public void init(Bundle savedInstanceState) {
        //logo
        setDrawerHeaderImage(R.drawable.menu_logo);

        MaterialSection section = newSection(getResources().getString(R.string.our_actions), f_actions);
        addSection(section);

        addSection(newSection(getResources().getString(R.string.notifications), f_notifications));

        addSection(newSection(getResources().getString(R.string.chat), f_chat));
        enableToolbarElevation();

        LoadEventsTask task = new LoadEventsTask(this);
        task.execute();
    }

    /**
     * Zde se nastavuje jaké menu bude použito v ActionBaru
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Only show items in the action bar relevant to this screen
        // if the drawer is not showing. Otherwise, let the drawer
        // decide what to show in the action bar.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Zde se nastavujou akce pro jednotlivé id z menu nastaveného v ActionBaru
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //ukazka pouziti SnackBaru
        if (item.getItemId() == R.id.action_example) {
            //Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT).show();
            SnackbarManager.show(
                    Snackbar.with(this)
                            .text(getResources().getString(R.string.example_text)));
            return true;
        }
        //ukazka pouziti AlertDialogu
        else if (item.getItemId() == R.id.action_settings) {
            //Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT).show();
            final AlertDialogWrapper.Builder builder = new AlertDialogWrapper.Builder(this);
            builder.setTitle(getResources().getString(R.string.example_title));
            builder.setCancelable(false);
            builder.setMessage(getResources().getString(R.string.example_text));
            builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog2, int which) {
                    dialog2.dismiss();
                }
            });
            Dialog d = builder.create();
            d.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Řekl bych že Vojtova záležitost...
     */
    private class LoadEventsTask extends AsyncTask<Void, Integer, Integer> {

        ProgressDialog dialog;
        TextView header;
        Context c;
        EventService es;

        public LoadEventsTask(Context context) {
            c = context;
        }

        @Override
        public void onPreExecute() {

            Log.e("AsyncTask", "onPreExecute");

            dialog = new ProgressDialog(c);
            dialog.setMessage(getResources().getString(R.string.please_wait) + " ... " + getResources().getString(R.string.getting_data));
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.show();
        }

        @Override
        protected Integer doInBackground(Void... params) {

            int res = -1;


            es = new EventService();
            try {
                events = es.SelectEvent();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return 0;
            }

            return res;
        }

        public void onPostExecute(Integer result) {
            dialog.dismiss();

            if (result != 0){
                //Toast.makeText(getApplicationContext(), getResources().getString(R.string.loading_ok), Toast.LENGTH_SHORT).show();
                f_actions.tv.setText(MainActivity.events.getFirst().getPost_title());
                SnackbarManager.show(
                    Snackbar.with(c)
                            .text(getResources().getString(R.string.loading_ok)));
            }
            else {
                //Toast.makeText(getApplicationContext(), getResources().getString(R.string.cant_load_data), Toast.LENGTH_SHORT).show();
                SnackbarManager.show(
                        Snackbar.with(c)
                                .text(getResources().getString(R.string.cant_load_data)));
            }
            Log.d("AsyncTask", "onPostExecute");

        }
    }
}
