package com.example.iri.vkh_app;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Iri on 8. 4. 2015.
 */
public class Fragment_actions extends Fragment{

    public TextView tv;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        tv = (TextView) rootView.findViewById(R.id.textView1);

        return rootView;
    }
}
