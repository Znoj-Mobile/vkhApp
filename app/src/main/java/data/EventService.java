package data;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class EventService {
	private DataProvider dataprovider;

	public EventService() {
		dataprovider = new DataProvider();
	}
	
	/**
	 * SelecetEvent vol� server a stahuje ud�losti a ukl�d� je do LinkedListu
	 * @return LinkedList s ud�lostmi, se�azen� podle akce_od
	 */
	public LinkedList<Event> SelectEvent() throws Exception {
		ArrayList<NameValuePair> values = new ArrayList<NameValuePair>();

		values.add(new BasicNameValuePair("exe", "select_all"));
		String result = dataprovider.readInputStream(dataprovider.getResponse( "http://vysokoskolaci.info/test/EventExecute.php", values));
		
		LinkedList<Event> events = new LinkedList<Event>();

			
		if (result != "error") {
			
			/**
			 * 3��dky kv�li o�et�en� JSON - pos�lal 9 n�jak�ch bytu, kvuli kter�m to ne�lo parsovat.
			 */
			byte[] help = result.getBytes("UTF-8");
			byte[] help1 = Arrays.copyOfRange(help, 9, help.length);
			result =  new String(help1, "UTF-8");
			
			JSONArray jArray = new JSONArray(result);

			for (int i = 0; i < jArray.length(); i++) {
				JSONObject dataEvent = jArray.getJSONObject(i);

				int id = dataEvent.getInt("id");
				String post_title = dataEvent.getString("post_title");
				String post_content = dataEvent.getString("post_content");
				String guid = dataEvent.getString("guid");
				String akce_od = dataEvent.getString("akce_od");
				String akce_do = dataEvent.getString("akce_do");

				Event event = new Event(id, post_title, post_content, guid, akce_od, akce_do );
				
				events.add(event);
			}
		}

		return events;
	}
}
