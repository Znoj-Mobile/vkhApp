package data;

import android.media.Image;

public class Event {

	private int id;
	private String post_title, post_content, guid, akce_od, akce_do;
	private Image image;
	
	public Event(int id, String post_title, String post_content, String guid, String akce_od, String akce_do)
	{
		this.id = id;
		this.post_title = post_title;
		this.post_content = post_content;
		this.guid = guid;
		this.akce_od = akce_od;
		this.akce_do = akce_do;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPost_title() {
		return post_title;
	}

	public void setPost_title(String post_title) {
		this.post_title = post_title;
	}

	public String getPost_content() {
		return post_content;
	}

	public void setPost_content(String post_content) {
		this.post_content = post_content;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getAkce_od() {
		return akce_od;
	}

	public void setAkce_od(String akce_od) {
		this.akce_od = akce_od;
	}

	public String getAkce_do() {
		return akce_do;
	}

	public void setAkce_do(String akce_do) {
		this.akce_do = akce_do;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
}
