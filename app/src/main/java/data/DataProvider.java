package data;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;


public class DataProvider {


	/**
	 * This method sends data on the server and returns response of server as
	 * object of InputStream class.
	 * 
	 * @param PAGE
	 *            URL of PHP script on the server.
	 * @param nameValuePairs
	 *            Data, which you want send to the server.
	 * @return Server response as object of InputStream class.
	 * @throws java.io.IOException
	 *             When some error during communication has occurred.
	 */
	public InputStream getResponse(String PAGE,
			ArrayList<NameValuePair> nameValuePairs) throws IOException {

		String entityValue = URLEncodedUtils.format(nameValuePairs, HTTP.UTF_8);
		PAGE += "?" + entityValue;

		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(PAGE);

		HttpResponse httpResponse = httpClient.execute(httpPost);
		HttpEntity httpEntity = httpResponse.getEntity();
		return httpEntity.getContent();
	}

	/**
	 * Converts an object of InputStream into a normal string and returns it.
	 *
	 * @param is
	 *            Input value of object InputStream.
	 * @return Converted string from object of InputStream.
	 * @throws java.io.IOException
	 *             When some error has occurred.
	 */
	public String readInputStream(InputStream is) throws IOException {
		// Maximum buffer size
		final int BUFFER_SIZE = 2048;

		char[] buf = new char[BUFFER_SIZE];
		Reader r = new InputStreamReader(is, "UTF-8");
		StringBuilder s = new StringBuilder();
		while (true) {
			int n = r.read(buf);
			if (n < 0)
				break;
			s.append(buf, 0, n);
		}
		return s.toString();
	}
}